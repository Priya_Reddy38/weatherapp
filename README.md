# WeatherApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.

#Summary
This application is used to know the weather details of 5 European cities.

#DevWalkthrough

This application is built using Angular 9. SPA approach is used making all the parts of the application as components, which can be reused. 
Git commands are used for version control. 
Bootstrap Grid system cards and flexbox are implemented.
Javascript: ES6 arrow functions,let,const,Array methods are used.
CSS preprocessors : SCSS is used for imports,functions and variables.
Used flexbox for alignment.
Typescript: Importing,exporting classes, datatypes specified.
Used dataService as making API calls for getting the data.
Used pipes for formatting the data using moment.js.
RxJS Observables for receiving the data from API response.
Services(Angular): Business logic which can be reused across all components like interacting with backend APIs are implemented.
TSLint : tslint is used for making the code readability,maintainability
Test cases for making a DataService API call.

Third Party Libraries::
Angular Material
Bootstrap
Moment.js



