import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { OnInit } from '@angular/core';
import { DataServiceService } from '../service/data-service.service';
import { DailyWeatherComponent } from '../daily-weather/daily-weather.component';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public weatherArray = [];
  public dailyWeather = DailyWeatherComponent;
  public showMore: boolean;
  public nextFiveDay: any;

  constructor(public dataService: DataServiceService) { }
  ngOnInit() {

    this.dataService.get(this.dataService.apiBaseUrl + '2.5/find?lat=55.5&lon=37.5&cnt=5&appid=' + this.dataService.apiKey, (data) => {
      console.log(data);
      this.weatherArray = data.list;
    });

  }

  onClickCity(cityName) {
    this.showMore = false;
    this.dataService.get(this.dataService.apiBaseUrl + '2.5/forecast?q=' + cityName + '&appid=' + this.dataService.apiKey,
      (data) => {
        this.prepareNextDay(data);
      });
  }
  prepareNextDay(data) {
    this.nextFiveDay = data.list.filter((weather) => weather.dt_txt.includes("09:00:00"));
    this.dataService.nextFiveDay = this.nextFiveDay;
    this.showMore = true;
    //console.log(this.nextFiveDay);
  }

}
