import { Component, OnInit } from '@angular/core';
import { DataServiceService } from '../service/data-service.service';
import * as moment from 'moment';
@Component({
  selector: 'app-daily-weather',
  templateUrl: './daily-weather.component.html',
  styleUrls: ['./daily-weather.component.scss']
})
export class DailyWeatherComponent implements OnInit {
  public dailyWeather: any;

  constructor(public dataService: DataServiceService) { }

  ngOnInit(): void {
    this.dailyWeather = this.dataService.nextFiveDay;
  }
  getDayName(date) {
    var dt = moment(date, "YYYY-MM-DD HH:mm:ss");
    return dt.format('ddd D');
  }
}
