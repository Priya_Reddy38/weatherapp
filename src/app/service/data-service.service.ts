import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class DataServiceService {
  public nextFiveDay;
  public apiBaseUrl;
  public apiKey;
  constructor(private http: HttpClient) {
    this.apiBaseUrl = 'https://api.openweathermap.org/data/';
    this.apiKey = '3d8b309701a13f65b660fa2c64cdc517';
  }

  get(uri?, callback?) {
    this.http.get(uri).subscribe(
      data => callback(data)
    );
  }
  getImageSrc(iconName) {
    return 'https://openweathermap.org/img/wn/' + iconName + '.png';
  }
}
